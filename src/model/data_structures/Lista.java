package model.data_structures;

public interface Lista<E> extends Iterable<E>
{
	public void addFirst(E elem);
	public void append(E elem);
	
	public void removeFirst();
	public void removeLast();
	public void remove(int pos);
	
	public E get(int pos);
	public int size();
	public boolean isEmpty();
	
}
