package model.data_structures;

public class LinkedList<E> implements Lista<E>
{
	private Node<E> cabeza;
	private Node<E> cola;
	private int listSize;
	
	public LinkedList() 
	{
		cabeza = null;
		cola = null;
	}
	
	public LinkedList(E elem)
	{
		cabeza = new Node<E>(elem);
		cola  = cabeza;
		listSize = 1;
	}
	
	public void addFirst(E elem) 
	{
		Node<E> nuevaCabeza = new Node<E>(elem);
		if (isEmpty())
		{
			cabeza = nuevaCabeza;
			cola = cabeza;
			listSize ++;
		}
		else
		{
			nuevaCabeza.setNextNode(cabeza);
			cabeza = nuevaCabeza;
			listSize ++;
		}
	}

	public void append(E elem) 
	{
		Node<E> nuevaCola = new Node<E>(elem);
		if (isEmpty())
		{
			cabeza = nuevaCola;
			cola = cabeza;
			listSize ++;
		}
		else
		{
			cola.setNextNode(nuevaCola);
			cola = nuevaCola;
			listSize ++;
		}
	}

	
	public void removeFirst() 
	{
		if (isEmpty());
		else if (size() == 1)
		{
			cabeza = null;
			cola = null;
			listSize --;
		}
		else
		{
			Node<E> cabezaVieja = cabeza;
			cabeza = cabeza.getNext();
			cabezaVieja.setNextNode(null);
			listSize --;
		}
	}
	
	public void removeLast()
	{
		if (isEmpty());
		else if (size() == 1)
		{
			cabeza = null;
			cola = null;
			listSize --;
		}
		else
		{
			int i = 0;
			Node<E> nuevaCola = cabeza;
			while (i < size()-2)
			{
				nuevaCola = nuevaCola.getNext();
				i ++;
			}
			nuevaCola.setNextNode(null);
			cola = nuevaCola;
			listSize --;
		}
	}
	
	public void remove(int pos) 
	{
		if (isEmpty());
		else if (size() == 1)
		{
			cabeza = null;
			cola = null;
			listSize --;
		}
		else if (pos == 0)
		{
			removeFirst();
		}
		else if (pos == size()-1)
		{
			removeLast();
		}
		else
		{
			int i = 0;
			Node<E> actual = cabeza;
			Node<E> anterior = null;
			while (i < pos)
			{
				anterior = actual;
				actual = actual.getNext();
				i ++;
			}
			anterior.setNextNode(actual.getNext());
			actual.setNextNode(null);
			
			listSize --;
		}
	}
	
	public void removeAll()
	{
		cabeza = null;
		cola = null;
		listSize = 0;
	}

	@Override
	public E get(int pos) 
	{
		if (pos == 0)
		{
			return cabeza.getItem();
		}
		else if  (pos == size()-1)
		{
			return cola.getItem();
		}
		else
		{
			int i = 0;
			Node<E> actual = cabeza;
			while (i < pos)
			{
				actual = actual.getNext();
				i ++;
			}
			return actual.getItem();
		}
	}

	@Override
	public int size() 
	{
		return listSize;
	}

	@Override
	public boolean isEmpty() 
	{
		if (size() == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public Iterator<E> iterator()
	{
		return new Iterator<E>() 
		{
			Node <E> act = null;
			public boolean hasNext()
			{
				if (listSize == 0)
				{
					return false;
				}	
				if (act == null)
				{
					return true;
				}
				return act.getNext() != null;
			}
			public E next()
			{
				if (act == null)
				{
					act = cabeza;
				}	
				else
				{
					act = act.getNext();
				}
				return act.getItem();
			}
		};
	}

}
