package model.data_structures;

import java.util.EmptyStackException;

public class Queue <E> implements IQueue<E>
{
	private Node<E> primero;
	private Node<E> ultimo;
	private int size;
	
	public Queue()
	{
		primero = null;
		ultimo = null;
		size = 0;
	}
	
	@Override
	public void enqueue(E pElem) 
	{
		Node <E> nuevoNodo = new Node<E>(pElem);
		if (size == 0)
		{
			primero = nuevoNodo;
			ultimo = nuevoNodo;
		}
		else
		{
			Node<E> viejoUltimo = ultimo;
			viejoUltimo.setNextNode(nuevoNodo);
			ultimo = nuevoNodo;
		}
		size ++;
	}

	@Override
	public E dequeue() 
	{
		if (size == 0)
		{
			throw new EmptyStackException();
		}
		Node<E> primeroViejo = primero;
		E item = primero.getItem();
		primero = primeroViejo.getNext();
		primeroViejo.setNextNode(null);
		size --;
		return item;
	}
	
	public int size()
	{
		return size;
	}
	
	public Node darPrimero()
	{
		return primero;
	}
	
	public Node darUltimo()
	{
		return ultimo;
	}

	public Iterator<E> iterator()
	{
		return new Iterator<E>() 
		{
			Node <E> act = null;
			public boolean hasNext()
			{
				if (size == 0)
				{
					return false;
				}	
				if (act == null)
				{
					return true;
				}
				return act.getNext() != null;
			}
			public E next()
			{
				if (act == null)
				{
					act = primero;
				}	
				else
				{
					act = act.getNext();
				}
				return act.getItem();
			}
		};
	}

}
