package model.data_structures;

public class Node <E>
{
	private Node <E> next;
	private E item;
	
	public Node (E pItem)
	{
		next = null;
		item = pItem;
	}
	
	public Node<E> getNext()
	{
		return next;
	}
	
	public void setNextNode(Node<E> pNext)
	{
		next = pNext;
	}
	
	public E getItem ()
	{
		return item;
	}
	
	public void setItem (E pItem)
	{
		item = pItem;
	}
}
