package model.data_structures;

public class RedBlackBST<K extends Comparable<K>,V> {

	private static final boolean RED = true;
	private static final boolean BLACK = false;

	public Node root;

	public class Node
	{
		K key; // key
		V val; // associated data
		Node left, right; // subtrees
		int size; // # nodes in this subtree
		boolean color; // color of link from
		// parent to this node
		public Node(K key, V val, int N, boolean color){
			this.key = key;
			this.val = val;
			this.size = N;
			this.color = color;
		}
		
		public V darVal()
		{
			return val;
		}
		
		public Node darIzq()
		{
			return left;
		}
		
		public Node darDer()
		{
			return right;
		}
		
		public boolean esRojo()
		{
			return color;
		}
	}
	
	public Node darRaiz()
	{
		return root;
	}
	
	private boolean isRed(Node x)
	{
		if (x == null)
			return false;
		return x.color == RED;
	}

	private Node rotateLeft(Node h)
	{
		Node x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = h.color;
		h.color = RED;
		x.size = h.size;
		h.size = 1 + size(h.left) + size(h.right);
		return x;
	}

	private Node rotateRight(Node h)
	{
		Node x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = h.color;
		h.color = RED;
		x.size = h.size;
		h.size = 1 + size(h.left) + size(h.right);
		return x;
	}

	private void flipColors(Node h)
	{
		h.color = RED;
		h.left.color = BLACK;
		h.right.color = BLACK;
	}


	public RedBlackBST()
	{
		root=null;
	}

	public int size()	{ return size(root); }
	private int size(Node x)
	{
		if (x == null) return 0;
		return x.size;
	}

	public boolean isEmpty()
	{
		return root==null;
	}

	public V get(K key)
	{
		Node x = root;
		while (x != null)
		{
			int cmp = key.compareTo(x.key);
			if (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else return x.val;
		}
		return null;
	}

	public int getHeight(K key)
	{
		int num=0;
		if(contains(key))
		{
			boolean k=false;
			while(k)
			{
				Node x = root;
				num++;
				int cmp = key.compareTo(x.key);
				if (cmp < 0) x = x.left;
				else if (cmp > 0) x = x.right;
				else k=true;
			}		
			return num;
		}
		return -1;
	}

	public boolean contains(K key)
	{
		Iterator keys= keys();
		while(keys.hasNext())
		{
			K ll=(K)keys.next();
			if(ll==key) return true;
		}
		return false;
	}

	public void put(K key, V val) {
		if (key == null || val==null) throw new IllegalArgumentException("first argument to put() is null");
		root = put(root, key, val);
		root.color = BLACK;
		//assert check();
	}

	private Node put(Node h, K key, V val) {
		if (h == null)
			return new Node(key, val, 1, RED);
		int cmp = key.compareTo(h.key);
		if (cmp < 0)
			h.left = put(h.left, key, val);
		else if (cmp > 0)
			h.right = put(h.right, key, val);
		else
			h.val = val;
		// fix-up any right-leaning links
		if (isRed(h.right) && !isRed(h.left)) h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right)) flipColors(h);
		h.size = size(h.left) + size(h.right) + 1;
		return h;
	}

	public int height()	{return height(root);}

	private int height(Node h)
	{
		if(h == null) return 0;
		else return 1 +(Math.max(height(h.left),height(h.right)));
	}

	public K min(){	return min(root);}

	private K min(Node h)
	{
		if(h.size==0)return null;
		else if(h.size==1)return h.key;
		else return min(h.left);
	}

	public K max(){	return max(root);}

	private K max(Node h)
	{
		if(h.size==0)return null;
		else if(h.size==1)return h.key;
		else return max(h.right);
	}

	public boolean check(){ return check(root);	}
	private boolean check(Node h)
	{
		boolean c=true;
		while(h!=null && h.left!=null && h.right!=null) {
			//a
			int cmp = h.key.compareTo(h.left.key);
			if (cmp > 0) return false;

			//b
			cmp = h.key.compareTo(h.right.key);
			if (cmp < 0) return false;

			//c
			if(h.right.color==RED)return false;

			//d
			if(h.left!=null && h.left.left!=null && h.left.color==RED && h.left.left.color==RED)return false;

			//e
			int derecho=blackDer(h);
			int izquierdo=blackIzq(h);
			if(derecho!=izquierdo)return false;
			
			c=check(h.left);
			c=check(h.right);
		}
		return c;
	}
	private int blackIzq(Node h)
	{
		int izq=0;
		if(h.left!=null && h.left.color==BLACK)
		{
			izq=1+blackDer(h.left);
		}
		return izq;
	}
	
	private int blackDer(Node h)
	{
		int derecho=0;
		if(h.right!=null && h.right.color==BLACK)
		{
			derecho=1+blackDer(h.right);
		}
		return derecho;
	}

	public Iterator<K> keys()
	{
		Queue<K> q = new Queue<K>();
		inorder(root, q);
		return q.iterator();
	}
	private void inorder(Node x, Queue q)
	{
		if (x == null) return;
		inorder(x.left, q);
		q.enqueue(x.key);
		inorder(x.right, q);
	}
	
//	private void inorderVals(Node x, Queue q, V in, V end)
//	{
//		if (x == null) return;
//		inorderVals(x.left, q, in, end);
//		if (x.darVal().compareTo(in) >= 0 && x.darVal().compareTo(end) <= 0)
//		{
//			q.enqueue(x.darVal());
//		}
//		inorderVals(x.right, q, in, end);
//	}
	
	private void inorderKeys(Node x, Queue q, K in, K end)
	{
		if (x == null) return;
		inorderKeys(x.left, q, in, end);
		if (x.key.compareTo(end) <= 0)
		{
			if (x.key.compareTo(in) >= 0)
			{
				q.enqueue(x.darVal());
			}
			inorderKeys(x.right, q, in, end);
		}
	}

//	public Iterator<V> valuesInRange(V init, V end)
//	{
//		Queue <V> q= new Queue<V>();
//		inorderVals(root, q, init, end);
//		return q.iterator();
//	}
//	
	public Iterator<K> keysInRange(K init, K end)
	{
		Queue <K> q= new Queue<K>();
		inorderKeys(root, q, init, end);
		return q.iterator();
	}
	
	public Iterator<V> valuesOfKeysInRange(K init, K end)
	{
		Queue <V> q= new Queue<V>();
		inorderKeys(root, q, init, end);
		return q.iterator();
	}

}
