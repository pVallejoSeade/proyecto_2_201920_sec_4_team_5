package model.data_structures;

import java.util.Collections;
import model.data_structures.Iterator;


public class LinearProbingHashT <K,V> {
	private	int M ; // number of chains
	private int N; //Pairs in the table
	private	K[] keys;
	private LinkedList<V>[]vals;
	private int numRehashes;

	public LinearProbingHashT(int m)
	{
		M=m;
		keys = ( K[ ]) new Object[M];
		vals = ( LinkedList<V>[ ]) new LinkedList[M];
		numRehashes = 0;
	}

	public int darN()
	{
		return N;
	}

	public int darM()
	{
		return M;
	}

	private	int hash(K key ) { return (key.hashCode () & 0x7fffffff) % M ;}

	public	void put(K key , V val)
	{
		if	(N >= (3*M)/4)
			resize(2*M ); // double M
		int	i=hash(key);
		for(i = hash(key); keys[ i ] != null ; i = (i + 1) % M) {
			if(	keys[ i ].equals( key ))
			{
				vals[i]=null;
				vals[ i ]=new LinkedList<V>(val);
				return;
			}
		}
		keys[ i ] = key;
		vals[ i ]=new LinkedList<V>(val);
		N++;
	}

	public void putInSet(K key, V val)
	{
		if	(N >= (3*M)/4)
			resize(2*M ); // double M

		int	i;		
		for(i = hash(key); keys[ i ] != null ; i = (i + 1) % M) {
			if(	keys[ i ].equals( key ))
			{
				vals[ i ].append(val);
				N++;
				return;
			}
		}
		put(key,val);
	}

	public	V get(K key ) {
		for	(int i = hash(key); keys[ i ] != null ; i = (i + 1) % M)
			if	(keys[ i ].equals( key )) return vals[i].get(0);
		return	null;
	}

	public Iterator<V> getSet(K key)
	{
		int i = hash(key);
		if (keys[i] == null)
		{
			return new Iterator<V>() 
			{
				public boolean hasNext(){return false;}
				public V next(){return null;}
			};
		}
		else
		{
			return vals[i].iterator();
		}
	}

	public	V delete(K key) {
		if	(!contains(key)) return null;
		int	i = hash(key);
		while (!key.equals ( keys[i]))
			i	= (i + 1) % M;
		V val=vals[i].get(0);
		keys[i ] = null;
		vals [i ] = null;
		i = ( i + 1) % M;
		while	(keys[ i ] != null ) {
			K keyToRedo = keys[i];
			V valToRedo = vals[i].get(0);
			keys[ i ] = null;
			vals[ i ] = null;
			N--;
			put(keyToRedo , valToRedo);
			i	=( i + 1) % M;
		}
		N--;
		if	(N > 0 && N == M/8) resize(M/2);
		return val;
	}

	public Iterator<V> deleteSet(K key)
	{
		int i=hash(key);
		if (keys[i] == null)
		{
			return new Iterator<V>() 
			{
				public boolean hasNext(){return false;}
				public V next(){return null;}
			};
		}
		else {
			LinkedList<V> values=vals[i];
			vals[i]=null;
			return values.iterator();
		}
	}

	private boolean contains(K key) 
	{
		int i = hash(key);
		return keys[i] != null;
	}


	public void resize(int size)
	{
		K[] copiaK= keys;
		LinkedList<V>[] copiaV= vals;
		keys=(K[])new Object[size];
		vals=(LinkedList<V>[])new LinkedList[size];
		for(int i=0;i<copiaK.length;i++)
		{
			for(int j=0;copiaV[i]!=null && j<copiaV[i].size();j++) {
				putInSet(copiaK[i],copiaV[i].get(j));
				N--;}
		}
		M=size;
		copiaK=null;
		copiaV=null;
		
		numRehashes ++;
	}

	public Iterator<K> keys()
	{
		LinkedList<K> ret = new LinkedList<>();
		int i = 0;
		while (i < keys.length)
		{
			if (keys[i] != null)
			{
				ret.append(keys[i]);
			}
		}
		return ret.iterator();
	}
	
	public int darNumRehash()
	{
		return numRehashes;
	}
}
