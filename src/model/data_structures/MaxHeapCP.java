package model.data_structures;
import java.util.Arrays;
import java.util.Comparator;

public class MaxHeapCP<K>
{
	private K[] pq;
	private int n;                       
	private Comparator<K> comparator;  

	public MaxHeapCP() 
	{
		pq = (K[]) new Object[2];
		n = 0;
	}
	
	public static void sort(Comparable[] pq) 
	{
        int n = pq.length;
        for (int k = n/2; k >= 1; k--)
            sink(pq, k, n);
        while (n > 1) {
            exch(pq, 1, n--);
            sink(pq, 1, n);
        }
    }

	public boolean esVacia() 
	{
		return n == 0;
	}

	public int darNumElementos() 
	{
		return n;
	}

	public K max() 
	{
		if (esVacia()) return null;
		return pq[1];
	}

	private void resize(int capacity) 
	{
		assert capacity > n;
		K[] temp = (K[]) new Object[capacity];
		for (int i = 1; i <= n; i++) {
			temp[i] = pq[i];
		}
		pq = temp;
	}

	public void agregar(K x) 
	{

		if (n == pq.length - 1) resize(2 * pq.length);

		pq[++n] = x;
		swim(n);
		assert isMaxHeap();
	}

	public K sacarMax() 
	{
		if (esVacia()) return null;
		K max = pq[1];
		exch(1, n--);
		sink(1);
		pq[n+1] = null;  
		if ((n > 0) && (n == (pq.length - 1) / 4)) resize(pq.length / 2);
		assert isMaxHeap();
		return max;
	}

	private void swim(int k) 
	{
		while (k > 1 && less(k/2, k)) {
			exch(k, k/2);
			k = k/2;
		}
	}

	private void sink(int k) 
	{
		while (2*k <= n) {
			int j = 2*k;
			if (j < n && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}
	
	private static void sink(Comparable[] pq, int k, int n) 
	{
        while (2*k <= n) {
            int j = 2*k;
            if (j < n && less(pq, j, j+1)) j++;
            if (!less(pq, k, j)) break;
            exch(pq, k, j);
            k = j;
        }
    }

	private boolean less(int i, int j) 
	{
		if (comparator == null) {
			return ((Comparable<K>) pq[i]).compareTo(pq[j]) < 0;
		}
		else {
			return comparator.compare(pq[i], pq[j]) < 0;
		}
	}
	
	private static boolean less(Comparable[] pq, int i, int j) 
	{
        return pq[i-1].compareTo(pq[j-1]) < 0;
    }

	private void exch(int i, int j) 
	{
		K swap = pq[i];
		pq[i] = pq[j];
		pq[j] = swap;
	}
	
	private static void exch(Object[] pq, int i, int j) 
	{
        Object swap = pq[i-1];
        pq[i-1] = pq[j-1];
        pq[j-1] = swap;
    }

	private boolean isMaxHeap() 
	{
		for (int i = 1; i <= n; i++) 
		{
			if (pq[i] == null) return false;
		}
		for (int i = n+1; i < pq.length; i++) 
		{
			if (pq[i] != null) return false;
		}
		if (pq[0] != null) return false;
		return isMaxHeapOrdered(1);
	}

	private boolean isMaxHeapOrdered(int k) 
	{
		if (k > n) return true;
		int left = 2*k;
		int right = 2*k + 1;
		if (left  <= n && less(k, left))  return false;
		if (right <= n && less(k, right)) return false;
		return isMaxHeapOrdered(left) && isMaxHeapOrdered(right);
	}
}