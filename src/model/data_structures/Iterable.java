package model.data_structures;

public interface Iterable<E> 
{
	Iterator<E> iterator();
}
