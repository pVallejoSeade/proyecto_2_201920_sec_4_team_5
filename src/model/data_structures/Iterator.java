package model.data_structures;

public interface Iterator<E> 
{
	boolean hasNext();
	E next();
}
