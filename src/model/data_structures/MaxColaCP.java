package model.data_structures;
import java.lang.Comparable;

import model.data_structures.ArregloDinamico;

public class MaxColaCP<T extends Comparable<T>> 
{
	private int N;
	private T[] pq; // pq[i] = ith element on pq

	public MaxColaCP (int capacity) 
	{

		pq = (T[]) new Comparable [ capacity ]; 
		N=0;
	}

	private boolean less (int i, int j)
	{
		return pq[i].compareTo(pq[j]) < 0;
	}
	private void exch (int i, int j)
	{
		T t = pq[i];
		pq[i] = pq[j];
		pq[j] = t;
	}

	public int darNumElementos()
	{
		return N;
	}

	public void agregar(T elemento)
	{
		pq[N++] = elemento;
	}

	public T sacarMax ()
	{
		if(esVacia()) return null;
		T last=darMax();
		N--;
		return last;
	}

	public T darMax()
	{
		if(esVacia()) return null;
		int max = 0;
		for (int i = 1; i < N; i++)
			if (less(max, i)) max = i;
		exch(max, N-1);
		return pq[N-1];
	}

	public boolean esVacia ()
	{
		return N==0;
	}

}
