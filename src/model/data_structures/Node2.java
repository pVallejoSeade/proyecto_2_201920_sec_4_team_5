package model.data_structures;

public class Node2<K, V> 
{
	private K key;
	private V value;
	private Node2<K, V> next;
	private Node2<K, V> previous;
	
	public Node2(K pKey, V pValue)
	{
		key = pKey;
		value = pValue;
		next = null;
		previous = null;
	}
	
	public Node2(K pKey, V pValue, Node2<K, V> pNext, Node2<K, V> pPrevious)
	{
		key = pKey;
		value = pValue;
		next = pNext;
		previous = pPrevious;
	}
	
	public Node2<K, V> getNext()
	{
		return next;
	}
	
	public Node2<K, V> getPrevious()
	{
		return previous;
	}
	
	public boolean hasNext()
	{
		return (next != null);
	}
	
	public boolean hasPrevious()
	{
		return (previous != null);
	}
	
	public void setNextNode(Node2<K, V> pNext)
	{
		next = pNext;
	}
	
	public void setPreviousNode(Node2<K, V> pNext)
	{
		next = pNext;
	}
	
	public K darKey()
	{
		return key;
	}
	
	public V darValue()
	{
		return value;
	}
	
	public void setValue(V pValue)
	{
		value = pValue;
	}
}
