package model.data_structures;

public interface IQueue <E> extends Iterable<E> {
	
	public void enqueue(E pElem);
	public E dequeue ();
	public int size();
	public Node<E>darPrimero();
	public Node<E>darUltimo();
}
