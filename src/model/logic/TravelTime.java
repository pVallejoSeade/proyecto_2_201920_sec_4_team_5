package model.logic;

public class TravelTime implements Comparable<TravelTime>{

	private int trimestre;
	private int sourceid;
	private int dstid;
	private int time;
	private double meanTravelTime;
	private double deviationTravelTime;

	public TravelTime(int pTrimestre, int source, int ds, int pTime, double mean_travel_time, double stdev_travel_time)
	{
		trimestre=pTrimestre;
		sourceid=source;
		dstid=ds;
		time=pTime;
		meanTravelTime=mean_travel_time;
		deviationTravelTime=stdev_travel_time;
	}

	public int darTrimestre()
	{
		return trimestre;
	}

	public int darSourceID()
	{
		return sourceid;
	}

	public int darDstID()
	{
		return dstid;
	}

	public int darTiempo()
	{
		return time;
	}

	public double darMeanTravelTime()
	{
		return meanTravelTime;
	}

	public double darDevTravelTime()
	{
		return deviationTravelTime;
	}


	@Override
//	public int compareTo(TravelTime trip) {
//		if(trip.darDow()>dow)
//		{
//			return -1;
//		}
//		else if(trip.darDow()<dow)
//		{
//			return 1;
//		}
//		else {
//			if(trip.darDow()>dow)
//			{
//				return -1;
//			}
//			else if(trip.darDow()<dow)
//			{
//				return 1;
//			}
//			else {
//				return 0;
//			}
//		}
//	}
	public String toString()
	{
		return ""+this.darDstID();
	}

	@Override
	public int compareTo(TravelTime o) {
		return 0;
	}
}
