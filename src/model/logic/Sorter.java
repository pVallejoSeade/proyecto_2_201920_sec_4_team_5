package model.logic;

import java.util.Random;

public class Sorter {

	private static Random random= new Random();

	private static void exch(TravelTime[] a, int i, int j)
	{
		TravelTime swap = a[i];
		a[i] = a[j];
		a[j] = swap;
	}
	
	public static void shellSort(TravelTime[] arreglo)
	{
		int N = arreglo.length;
		int h = 1;
		while (h < N/3)
			h = 3*h + 1;
		while (h >= 1) {
			for (int i = h; i < N; i++) 
			{
				for(int j=i ; j>= h ; j -= h) 
				{
					if((arreglo[j].compareTo(arreglo[j-h])< 0))
						exch(arreglo,j,j-h);
					else 
						j=h-1;
				}

			}
			h = h/3;
		}
	}
		//QUICK

		private static int partition(TravelTime[] a, int lo, int hi)
		{ // Partition into a[lo..i-1], a[i], a[i+1..hi].
			int i = lo, j = hi+1; // left and right scan indices
			TravelTime v = a[lo]; // partitioning item
			while (true)
			{ // Scan right, scan left, check for scan complete, and exchange.
				while (a[++i].compareTo(v)<0) if (i == hi) break;
				while (v.compareTo (a[--j])<0) if (j == lo) break;
				if (i >= j) break;
				exch(a, i, j);
			}
			exch(a, lo, j); // Put v = a[j] into position
			return j; // with a[lo..j-1] <= a[j] <= a[j+1..hi].
		}


		private static void quickSort(TravelTime[] a,int lo, int hi) {
			if (hi <= lo) return;
			int j = partition(a, lo, hi);
			quickSort(a, lo, j-1); // Sort left part a[lo .. j-1].
			quickSort(a, j+1, hi); // Sort right part a[j+1 .. hi].
		}

		public static void shuffle(Object[] a) {
			long seed = System.currentTimeMillis();
			random = new Random(seed);
			if(a!=null)
			{
				int n = a.length;
				for (int i = 0; i < n; i++) {
					int r = i + random.nextInt(n-i);     // between i and n-1
					Object temp = a[i];
					a[i] = a[r];
					a[r] = temp;
				}
			}

		}


		public static void quickSort(TravelTime[] a) {
			shuffle(a); 
			quickSort(a, 0, a.length - 1);
		}
	}
