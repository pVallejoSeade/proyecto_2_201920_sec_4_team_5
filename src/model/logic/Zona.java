package model.logic;

import java.util.ArrayList;

import model.data_structures.LinkedList;

public class Zona {
	
	private String nombre;
	private double perimeter;
	private double area;
	private int id;
	ArrayList<String[]> coordinates;

	public Zona(String pNombre, double perim, double pArea, int pId, ArrayList<String[]> coord)
	{
		nombre=pNombre;
		perimeter=perim;
		area=pArea;
		id= pId;
		coordinates=coord;
	}
	
	public String darNombre()
	{
		return nombre;
	}
	
	public double darPerimetro()
	{
		return perimeter;
	}
	
	public double darArea()
	{
		return area;
	}
	
	public int darID()
	{
		return id;
	}
	
	public ArrayList<String[]> darCoordenadasFrontera()
	{
		return coordinates;
	}

}
