package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.opencsv.CSVReader;
import java.io.IOException;

import model.data_structures.Iterator;
import model.data_structures.LinearProbingHashT;
import model.data_structures.LinkedList;
import model.data_structures.Lista;
import model.data_structures.MaxColaCP;
import model.data_structures.MaxHeapCP;
import model.data_structures.RedBlackBST;


import java.lang.Comparable;

import java.lang.Math;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import org.junit.runner.manipulation.Sorter;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo 
{
	
	private LinkedList<TravelTime> listaMes1;
	private LinkedList<TravelTime> listaMes2;
	private LinkedList<TravelTime> listaSemana1;
	private LinkedList<TravelTime> listaSemana2;
	private LinkedList<TravelTime> listaHora;
	private LinkedList<NodeRedVial> listaNodosRedVial;
	private LinkedList<Zona> listaZonas;
	private static int MAX = 20;

	/**
	 * Constructor del modelo del mundo con capacidad predefinida
	 */
	public MVCModelo()
	{
		listaMes1 = new LinkedList<TravelTime>();
		listaMes2 = new LinkedList<TravelTime>();
		listaSemana1 = new LinkedList<TravelTime>();
		listaSemana2 = new LinkedList<TravelTime>();
		listaHora = new LinkedList<TravelTime>();
		listaNodosRedVial = new LinkedList<>();
		listaZonas = new LinkedList<>();
	}

	public void loadTravelTimes()
	{

		CSVReader reader = null;
		System.out.println("Archivos CSV");

		try {
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv"));
			reader.readNext();
			int viajes=0;
			for(String[] nextLine : reader) {
				int a = Integer.parseInt(nextLine[0]);
				int b = Integer.parseInt(nextLine[1]);
				int c = Integer.parseInt(nextLine[2]);
				double d = Double.parseDouble(nextLine[3]);
				double e = Double.parseDouble(nextLine[4]);

				TravelTime v= new TravelTime(1,a,b,c,d,e);
				String key= 1 + "-" + a + "-" + b;
				viajes++;
				listaSemana1.append(v);
			}

			System.out.println("Viajes 2018-1 weekly: " + viajes);
			System.out.println();


			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-WeeklyAggregate.csv"));
			reader.readNext();
			viajes=0;
			for(String[] nextLine : reader) {
				int a = Integer.parseInt(nextLine[0]);
				int b = Integer.parseInt(nextLine[1]);
				int c = Integer.parseInt(nextLine[2]);
				double d = Double.parseDouble(nextLine[3]);
				double e = Double.parseDouble(nextLine[4]);

				TravelTime v= new TravelTime(2,a,b,c,d,e);
				String key= 2 + "-" + a + "-" + b;
				viajes++;
				listaSemana2.append(v);
			}

			System.out.println("Viajes 2018-2 weekly: " + viajes);
			System.out.println();


			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			reader.readNext();
			viajes=0;
			for(String[] nextLine : reader) {
				int a = Integer.parseInt(nextLine[0]);
				int b = Integer.parseInt(nextLine[1]);
				int c = Integer.parseInt(nextLine[2]);
				double d = Double.parseDouble(nextLine[3]);
				double e = Double.parseDouble(nextLine[4]);

				TravelTime v= new TravelTime(1,a,b,c,d,e);
				String key= 1 + "-" + a + "-" + b;
				viajes++;
				listaHora.append(v);
				
			}
			
			System.out.println("Viajes 2018-1 hourly: " + viajes);
			System.out.println();


			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-HourlyAggregate.csv"));
			reader.readNext();
			viajes=0;
			for(String[] nextLine : reader) {
				int a = Integer.parseInt(nextLine[0]);
				int b = Integer.parseInt(nextLine[1]);
				int c = Integer.parseInt(nextLine[2]);
				double d = Double.parseDouble(nextLine[3]);
				double e = Double.parseDouble(nextLine[4]);

				TravelTime v= new TravelTime(2,a,b,c,d,e);
				String key= 2 + "-" + a + "-" + b;
				viajes++;
				listaHora.append(v);
		    }
			System.out.println("Viajes 2018-2 hourly: " + viajes);
			System.out.println();
			
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-MonthlyAggregate.csv"));
			reader.readNext();
			viajes=0;
			for(String[] nextLine : reader) {
				int a = Integer.parseInt(nextLine[0]);
				int b = Integer.parseInt(nextLine[1]);
				int c = Integer.parseInt(nextLine[2]);
				double d = Double.parseDouble(nextLine[3]);
				double e = Double.parseDouble(nextLine[4]);

				TravelTime v= new TravelTime(1,a,b,c,d,e);
				String key= 1 + "-" + a + "-" + b;
				viajes++;
				listaMes1.append(v);
				
		    }
			System.out.println("Viajes 2018-1 monthly: " + viajes);
			System.out.println();
			
			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-All-MonthlyAggregate.csv"));
			reader.readNext();
			viajes=0;
			for(String[] nextLine : reader) {
				int a = Integer.parseInt(nextLine[0]);
				int b = Integer.parseInt(nextLine[1]);
				int c = Integer.parseInt(nextLine[2]);
				double d = Double.parseDouble(nextLine[3]);
				double e = Double.parseDouble(nextLine[4]);

				TravelTime v= new TravelTime(2,a,b,c,d,e);
				String key= 2 + "-" + a + "-" + b;
				viajes++;
				listaMes2.append(v);
				
		    }
			System.out.println("Viajes 2018-2 monthly: " + viajes);
			System.out.println();
			
			System.out.println("Archivos JSON");

			int zonas=0;
			String filename = "./data/bogota_cadastral.json";
			JsonReader json = new JsonReader(new FileReader(filename));
			JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
			
			JsonArray features = jsonObject.get("features").getAsJsonArray();
			
			for (JsonElement element: features) 
			{
				JsonObject objeto = element.getAsJsonObject();
				JsonObject geometry = objeto.get("geometry").getAsJsonObject();
				JsonArray coordinates = geometry.get("coordinates").getAsJsonArray().get(0).getAsJsonArray().get(0).getAsJsonArray();
				ArrayList<String[]> coordi = new ArrayList<String[]>();
				for (JsonElement element1: coordinates)
				{
					String[] coor = new String[2];
					JsonArray objeto1 = element1.getAsJsonArray();
					coor[0] = objeto1.get(0).getAsString();
					coor[1] = objeto1.get(1).getAsString();
					coordi.add(coor);
				}
				JsonObject properties = objeto.get("properties").getAsJsonObject();
				String nombre = properties.get("scanombre").getAsString();
				double perimetro = properties.get("shape_leng").getAsDouble();
				double area = properties.get("shape_area").getAsDouble();
				int id = Integer.parseInt(properties.get("MOVEMENT_ID").getAsString());
				Zona z = new Zona(nombre, perimetro, area, id, coordi);
				listaZonas.append(z);
				zonas++;
				
			}
			System.out.println("Zonas: " + zonas);
			System.out.println();
			
			System.out.println("Archivo TXT");
			
			reader = new CSVReader(new FileReader("./data/Nodes_of_red_vial-wgs84_shp.txt"));
			int nodos=0;
			for(String[] nextLine : reader) {
				int id = Integer.parseInt(nextLine[0]);
				float lat = Float.parseFloat(nextLine[2]);
				float longi = Float.parseFloat(nextLine[1]);
				NodeRedVial n = new NodeRedVial(id, longi, lat);
				listaNodosRedVial.append(n);
				nodos++;
				
		    }
			System.out.println("Nodos: " + nodos);
			System.out.println();


		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}
	
	public LinkedList<TravelTime> darListaMes1()
	{
		return listaMes1;
	}
	
	public LinkedList<TravelTime> darListaMes2()
	{
		return listaMes2;
	}
	
	public LinkedList<TravelTime> darListaHora()
	{
		return listaHora;
	}
	
	public LinkedList<TravelTime> darListaSeman1()
	{
		return listaSemana1;
	}
	
	public LinkedList<TravelTime> darListaSeman2()
	{
		return listaSemana2;
	}
	
	public LinkedList<NodeRedVial> darListaNodosRedVial()
	{
		return listaNodosRedVial;
	}
	
	public LinkedList<Zona> darListaZonas()
	{
		return listaZonas;
	}
	
	public LinkedList<LinkedList<String>> req1A()
	{
		
		//Cargar la lista de zonas a una cola de prioridad
		Iterator<Zona> iter = listaZonas.iterator();
		MaxHeapCP<String> cola = new MaxHeapCP<>();
		while (iter.hasNext())
		{
			cola.agregar(iter.next().darNombre());
		}
		
		//Crear una lista de colas con los nombres que comienzen por la misma letra
		LinkedList<LinkedList<String>> listaColas = new LinkedList<>();
		LinkedList<String> listaLetra = null;
		char letra = '0';
		while (!cola.esVacia())
		{
			String nombre = cola.sacarMax();
			if (letra == Character.toUpperCase(nombre.charAt(0)))
			{
				listaLetra.addFirst(nombre);
			}
			else
			{
				letra = Character.toUpperCase(nombre.charAt(0));
				if (listaLetra != null)
				{
					listaColas.append(listaLetra);
				}
				listaLetra = new LinkedList<>();
				listaLetra.addFirst(nombre);
			}
		}
		
		return listaColas;
	}
	
	public LinkedList<String> req2A(String lat, String longi)
	{
		//Crea una lista con la informacion de los nodos cuya latitud y longitud sea igual a la de los parametros hasta el tercer decimal
		LinkedList<String> respuesta = new LinkedList<>();
		
		//Crea una linkedList de tablas de hash
		LinkedList<LinearProbingHashT<String, String>> listaDeTablas= new LinkedList<>();
		int i = 0;
		
		//Se recorre la lista principal de zonas creando una tabla de hash por cada zona llena de las coordenadas de la zona
		while (i < listaZonas.size())
		{
			Zona z = listaZonas.get(i);
			int m = Math.round((4*(listaZonas.get(i).darCoordenadasFrontera().size())/3));
			LinearProbingHashT<String, String> tablaHash = new LinearProbingHashT<>(m + 2);
			ArrayList<String[]> coordenadas = z.darCoordenadasFrontera();
			int j = 0;
			while (j < coordenadas.size())
			{
				//Crear la llave para cada elemento de la tabla
				String latitud = coordenadas.get(j)[1];
				String longitud = coordenadas.get(j)[0];
				String keyLat = latitud;
				while (keyLat.length() < 8)
				{
					if (keyLat.length() < 2)
					{
						keyLat += ".";
					}
					else
					{
						keyLat += "0";
					}
				}
				keyLat = keyLat.substring(0, 5);
				String keyLon = longitud;
				while (keyLon.length() < 10)
				{
					if (keyLon.length() < 4)
					{
						keyLon += ".";
					}
					else
					{
						keyLon += "0";
					}
				}
				keyLon = keyLon.substring(0, 7);
				String key = keyLat + "," + keyLon;
				
				//Crea el valor para el elemento en la tabla
				String value = key + ";" + z.darNombre() + " [" + latitud + ", " + longitud + "]";
				
				//Agrega elemento a la tabla
				tablaHash.putInSet(key, value);
				
				j ++;
			}
			//Agrega la tabla de hash de la zona a la lista de tablas de hash
			listaDeTablas.append(tablaHash);
			
			i ++;
		}
		
		//Crea la llave que se necesita buscar a partir de la informacion de los parametros
		String lat1 = lat;
		while (lat1.length() < 8)
		{
			if (lat1.length() < 2)
			{
				lat1 += ".";
			}
			else
			{
				lat1 += "0";
			}
		}
		lat1 = lat1.substring(0, 5);
		
		String longi1 = longi;
		while (longi1.length() < 10)
		{
			if (longi1.length() < 4)
			{
				longi1 += ".";
			}
			else
			{
				longi1 += "0";
			}
		}
		longi1 = longi1.substring(0, 7);
		
		String keyParaBuscar =  lat1 + "," + longi1;
		
		//Recorre la lista de tablas extrayendo la informacion de los nodos buscados
		i = 0;
		while (i < listaDeTablas.size())
		{
			Iterator<String> iter = listaDeTablas.get(i).getSet(keyParaBuscar);
			while (iter.hasNext())
			{
				String[] s = iter.next().split(";");
				if (s[0].compareTo(keyParaBuscar) == 0)
				{
					respuesta.append(s[1]);
				}
			}
			
			i ++;
		}
		
		return respuesta;
	}

	public Iterator<TravelTime> req3A(double tiempInferior, double tiempSuperior, int numViajes)
	{
		//Pasa toda la informacion a un arbol rojo y negro
		Iterator iter = listaMes1.iterator();
		RedBlackBST<String, TravelTime> arbol = new RedBlackBST<>();
		TravelTime v;
		while (iter.hasNext())
		{
			v = (TravelTime)iter.next();
			arbol.put(v.darMeanTravelTime() + "," + v.darSourceID() + "," + v.darDstID() + "," + v.darDevTravelTime(), v);
		}
		
		//Sacar los datos que estan en el rango dado
		iter = arbol.valuesOfKeysInRange(Double.toString(tiempInferior), Double.toString(tiempSuperior + 0.1));
		
		//Meter los datos en un nuevo arbol para asi organizar los datos por srcId y DstID
		arbol = new RedBlackBST<>();
		while(iter.hasNext())
		{
			v = (TravelTime)iter.next();
			arbol.put(v.darSourceID() + "," + v.darDstID() + "," + v.darMeanTravelTime() + "," + v.darDevTravelTime(), v);
		}
		
		//Sacar los datos del nuevo arbol
		if (arbol.isEmpty())
		{
			return null;
		}
		else
		{
			return arbol.valuesOfKeysInRange(arbol.min(), arbol.max());
		}
		
		
	}
	
//	Buscar los N zonas que est�n m�s al norte. N es un valor de entrada. Una zona A esta
//	m�s al Norte que una zona B si alg�n punto de la frontera de A est� m�s al norte que todos
//	los puntos de la frontera de B.
//	Mostrar las zonas ordenadas desde las que est�n m�s al norte. De cada zona se debe
//	imprimir el nombre y la (latitud, longitud) de su punto m�s al norte. 

	
	public void req1B(int numZonas)
	{
		MaxColaCP<String> nortes= new MaxColaCP(listaZonas.size());
		ArrayList<String> ls =new ArrayList(); //Areglo con los maximo de cada zona
		ArrayList<String> longs =new ArrayList(); //Arreglo con las longitudes de puntos mas al norte
		for(int i=0; i< listaZonas.size();i++)
		{
			ArrayList<String[]> act =listaZonas.get(i).darCoordenadasFrontera();
			
			MaxColaCP<String> latitudes= new MaxColaCP(act.size());
			
			//Meto a la la de prioridad las latitudes
			
			for(int j=0; j<act.size();j++)
			{
				String lat=act.get(j)[1];
				latitudes.agregar(lat);
			}
			//Busco el punto mas al norte de la zona
			String norte=latitudes.darMax();
			ls.add(norte);
			nortes.agregar(norte);	
			
			boolean ya=false;
			for(int j=0; j<act.size() && ya==false;j++)
			{
				String lat=act.get(j)[1];
				if(lat==norte) { longs.add(act.get(j)[0]); ya=true;}
			}
			
		}int n=0;
		for(int i=0;i<listaZonas.size() && n<numZonas ;i++)
		{
			String max= nortes.sacarMax();
			int k=0;
			while(k<listaZonas.size())
			{
				if(max== ls.get(k))
				{
					System.out.println(listaZonas.size());
					System.out.println(longs.size());
					System.out.println(nortes.darNumElementos());
					System.out.println(listaZonas.get(k).darNombre()+", ( "+max+", "+longs.get(k)+" )");
					k=listaZonas.size();
				}
				k++;
			}
			n++;
		}
	}
	
	
	public ArrayList<NodeRedVial> req2B(float lat, float longitud)
	{
		ArrayList<NodeRedVial> nodos = new ArrayList();
		int m = Math.round((4*(listaZonas.size())/3));
		LinearProbingHashT<Float, NodeRedVial> tabla= new LinearProbingHashT(m+2);
		for(int i=0; i<listaNodosRedVial.size(); i++)
		{
			NodeRedVial n =listaNodosRedVial.get(i);			
			float la =(float) (Math.floor(n.darLatitud() * 1e2) / 1e2);
			float lo =(float) (Math.floor(n.darLongitud() * 1e2) / 1e2);
			float llave=(lo + la) + (lo * la) + (lo/la); //No se si sea lo suficientemente inprobable de que la combinacion de ambas operaciones sea unica 
			tabla.putInSet(llave, n);			
		}		
		lat =(float) (Math.floor(lat * 1e2) / 1e2);
		longitud =(float) (Math.floor(longitud * 1e2) / 1e2);
		float ll=(longitud + lat) + (longitud * lat) + (longitud/lat);
		
		Iterator it =tabla.getSet(ll);
		while(it.hasNext())
		{
			NodeRedVial n= (NodeRedVial)it.next();
			nodos.add(n);
		}
		System.out.println("Nodos retornados: "+ nodos.size());
		return nodos;
	}
	
//	Dado un rango de desviaciones est�ndares [limite_bajo, limite_alto] retornar los viajes cuya
//	desviaci�n est�ndar mensual este en ese rango.
//	Se debe mostrar �nicamente N viajes ordenados por zona de origen y zona de destino. De
//	cada viaje se debe mostrar la zona de origen, zona de destino, mes y la desviaci�n est�ndar
//	del viaje
	public ArrayList<TravelTime> req3B(double DEInferior, double DESuperior, int numViajes)
	{		
		Iterator iter = listaMes1.iterator(); 
		RedBlackBST<String, TravelTime> arbol = new RedBlackBST<>();
		while (iter.hasNext())
		{
			TravelTime v = (TravelTime)iter.next();
			arbol.put(v.darDevTravelTime() + "," + v.darSourceID() + "," + v.darDstID() + "," + v.darMeanTravelTime(), v);
		}
		
		//Sacar los datos que estan en el rango dado
		iter = arbol.valuesOfKeysInRange(Double.toString(DEInferior), Double.toString(DESuperior + 0.1));
		
		//Meter los datos en un nuevo arbol para asi organizar los datos por srcId y DstID
		arbol = new RedBlackBST<>();
		while(iter.hasNext())
		{
			TravelTime v = (TravelTime)iter.next();
			arbol.put(v.darSourceID() + "," + v.darDstID() + "," + v.darMeanTravelTime() + "," + v.darDevTravelTime(), v);
		}
		
		//Sacar los datos del nuevo arbol
		if (arbol.isEmpty())
		{
			return null;
		}
		else
		{
			iter = arbol.valuesOfKeysInRange(arbol.min(), arbol.max());
			ArrayList<TravelTime> viajes= new ArrayList<TravelTime>();
			while(iter.hasNext() && viajes.size()<numViajes)
			{
				TravelTime t= (TravelTime)iter.next();
				viajes.add(t);
			}
			return viajes;
		}
	}
	
	public ArrayList<TravelTime> req1C(int idZona, int hora)
	{
		
		RedBlackBST<Integer, ArrayList<TravelTime>> arbol= new RedBlackBST<Integer, ArrayList<TravelTime>>();
		ArrayList<TravelTime>[] horas= new ArrayList[24]; 
		Iterator it=listaHora.iterator();
		
		for(int j=0; j<24;j++)
		{
			horas[j]=new ArrayList<TravelTime>();
		}
		
		
		while(it.hasNext())
		{
			TravelTime t= (TravelTime)it.next();
			horas[t.darTiempo()].add(t);
			
		}
		int i=0;
		for (ArrayList<TravelTime> arr: horas)
		{
			arbol.put(i, arr);
			i++;
		}		
		
		ArrayList<TravelTime>prov=arbol.get(hora) ;
		ArrayList<TravelTime> viajes= new ArrayList<TravelTime>();
		for(TravelTime v: prov)
		{
			if(v.darSourceID()==idZona) viajes.add(v);
		}
		
		
		return viajes;
	}
	
	public ArrayList<TravelTime> req2C(int zonaDestino, int horaSuperior, int horaInferior)//no funciona
	{		
		RedBlackBST<Integer, ArrayList<TravelTime>> arbol= new RedBlackBST<Integer, ArrayList<TravelTime>>();
		ArrayList<TravelTime>[] horas= new ArrayList[24]; 
		Iterator it=listaHora.iterator();
		
		for(int j=0; j<24;j++)
		{
			horas[j]=new ArrayList<TravelTime>();
		}
		
		
		while(it.hasNext())
		{
			TravelTime t= (TravelTime)it.next();
			horas[t.darTiempo()].add(t);
			
		}
		int i=0;
		for (ArrayList<TravelTime> arr: horas)
		{
			arbol.put(i, arr);
			i++;
		}		
		
		Iterator iter= arbol.valuesOfKeysInRange(horaInferior, horaSuperior);
		ArrayList<TravelTime> viajes= new ArrayList<TravelTime>();
		while(iter.hasNext())
		{
			ArrayList<TravelTime> ar= (ArrayList<TravelTime>)iter.next();
			for(TravelTime t: ar)
			{
				if(t.darDstID()==zonaDestino)viajes.add(t);
			}			
		}
		
		return viajes;
	}
	
	public LinkedList<String> req3C(int numZonas)
	{
		//Meter toda la informacion en un arbol rojo negro
		Iterator iter = listaZonas.iterator();
		RedBlackBST<Integer, String> arbol = new RedBlackBST<>();
		while (iter.hasNext())
		{
			Zona z = (Zona)iter.next();
			arbol.put(z.darCoordenadasFrontera().size()*-1, z.darNombre() + " (" + z.darCoordenadasFrontera().size() + " nodos)");
		}
		
		//Hacer la lista con la informacion de retorno
		LinkedList<String> retorno = new LinkedList<>();
		iter = arbol.valuesOfKeysInRange(-1000000000, 0);
		int i = 0;
		while (iter.hasNext() && i < numZonas)
		{
			retorno.append((String)iter.next());
			i ++;
		}
		
		return retorno;
	}
	
	public String req4C()
	{
		//Pasar la lista a un arbol rojo negro
		Iterator iter = listaHora.iterator();
		RedBlackBST<String, TravelTime> arbol = new RedBlackBST<>();
		TravelTime v;
		while (iter.hasNext())
		{
			v = (TravelTime)iter.next();
			arbol.put(v.darSourceID() + "," + v.darDstID() + "," + v.darTiempo() + "," + v.darTrimestre(), v);
		}
		
		//Pasar la informacion a una lista de listas, cada lista pequeña por una zona de origen
		int origen = 1;
		iter = arbol.valuesOfKeysInRange(arbol.min(), arbol.max());
		arbol = null;
		LinkedList<LinkedList<Integer>> listaGrande = new LinkedList<>();
		LinkedList<Integer> listaPequenia = new LinkedList<>();
		while (iter.hasNext())
		{
			v = (TravelTime)iter.next();
			if (v.darSourceID() == origen)
			{
				listaPequenia.append(v.darSourceID());
			}
			else
			{
				listaGrande.append(listaPequenia);
				listaPequenia = new LinkedList<>();
				origen = v.darSourceID();
				listaPequenia.append(v.darSourceID());
			}
		}
		
		//Hacer la tabla ASCII
		String retorno = "Porcentaje de datos faltantes por zona";
		iter = listaGrande.iterator();
		while (iter.hasNext())
		{
			listaPequenia = (LinkedList<Integer>)iter.next();
			retorno += "\n" + listaPequenia.get(0) + "|";
			int porcentaje = 100 - (listaPequenia.size()/55680)*100;
			int j = 0;
			while (j < porcentaje)
			{
				retorno += "*";
				j += 2;
			}
		}
		
		return retorno;
	}
}
