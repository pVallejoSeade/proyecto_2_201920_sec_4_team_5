package controller;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

import model.data_structures.Iterator;
import model.data_structures.LinkedList;
import model.data_structures.MaxHeapCP;
import model.logic.MVCModelo;
import model.logic.NodeRedVial;
import model.logic.TravelTime;
import model.logic.Zona;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";
		int i;
		TravelTime v;
		int numZonas;
		
		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
				case 1:
					System.out.println("--------- \nCargar viajes: ");
				    //int capacidad = lector.nextInt();
				    modelo = new MVCModelo(); 
				    modelo.loadTravelTimes();
					System.out.println("Datos cargados");
					System.out.println("---------");						
					break;
					
				case 2:
					System.out.println("--------- \nObtener las N letras más frecuentes por las que comienza el nombre de una zona: ");
					System.out.println("Ingresar número de letras que se desea obtener en la consulta:");
					int r = lector.nextInt();
					if (r > 20) {r = 20;}
					System.out.println("Resultados:");
					LinkedList<LinkedList<String>> req1a = modelo.req1A();
					i = 0;
					while (i < r)
					{
						//Busca la cola mas larga en la lista
						int j = 1;
						int posMax = 0;
						while (j < req1a.size())
						{
							if (req1a.get(j).size() > req1a.get(posMax).size())
							{
								posMax = j;
							}
							j ++;
						}
						System.out.println("\n" + (i+1) + ". Letra: " + req1a.get(posMax).get(0).charAt(0));
						j = 0;
						while (j < req1a.get(posMax).size())
						{
							System.out.println("\n\t"+ (j+1) + ". " + req1a.get(posMax).get(j));
							j ++;
						}
						req1a.remove(posMax);
						i ++;
					}
					System.out.println();
					System.out.println("---------");
					break;
					
				case 3:
					System.out.println("--------- \nObtener las N letras más frecuentes por las que comienza el nombre de una zona: ");
					System.out.println("Ingresar latitud:");
					String latitud = lector.next();
					System.out.println("Ingresar longitud:");
					String longitud = lector.next();
					LinkedList<String> req2a = modelo.req2A(latitud, longitud);
					System.out.println("Número de nodos retornados: " + req2a.size());
					if (req2a.isEmpty())
					{
						System.out.println("No hay nodos que se ajusten a esta informacion");
					}
					else
					{
						i = 0;
						while (i < req2a.size())
						{
							System.out.println("\n\t" + (i+1) + "." + req2a.get(i));
							
							i ++;
						}
					}
					
					System.out.println("\n---------");
					break;
					
				case 4:
					System.out.println("--------- \nBuscar los tiempos promedio de viaje que están en un rango y que son del primer trimestre del 2018: ");
					System.out.println("Ingresar tiempo inferior:");
					double tiempoInferior =lector.nextDouble();
					System.out.println("Ingresar tiempo superior:");
					double tiempoSuperior =lector.nextDouble();
					System.out.println("Ingresar número de viajes que se desea consultar:");
					int numViajes = lector.nextInt();
					if (numViajes > 20){numViajes = 20;}
					Iterator<TravelTime> req3a = modelo.req3A(tiempoInferior, tiempoSuperior, numViajes);
					if (req3a != null)
					{
						i = 1;
						while (req3a.hasNext() && i < (numViajes + 1))
						{
							v = req3a.next();
							System.out.println("Viaje " + i);
							System.out.println("\tZona de origen: " + v.darSourceID());
							System.out.println("\tZona de destino: " + v.darDstID());
							System.out.println("\tMes: " + v.darTiempo());
							System.out.println("\tTiempo promedio: " + v.darMeanTravelTime());
							i ++;
						}
					}
					else
					{
						System.out.println("No existen datos para la información suministrada.");
					}
					System.out.println("---------");
					break;
					
				case 5: 
					System.out.println("--------- \nBuscar los N zonas que están más al norte:");
					System.out.println("Ingresar número de viajes que se desea consultar:");
					numZonas = lector.nextInt();
					modelo.req1B(numZonas);
					System.out.println("---------");
					break;	
					
				case 6: 
					System.out.println("--------- \nBuscar nodos de la malla vial por Localización Geográfica (latitud, longitud):");
					System.out.println("Ingresar latitud:");
					float latitu = lector.nextFloat();
					System.out.println("Ingresar longitud:");
					float longitu = lector.nextFloat();
					ArrayList<NodeRedVial> nodo= modelo.req2B(latitu, longitu);
					for(NodeRedVial n: nodo)
					{
						System.out.println("Id: "+n.darId());
						System.out.println("Latitud: "+n.darLatitud());
						System.out.println("Longitud: "+n.darLongitud());
					}
					System.out.println("---------");
					break;	
					
				case 7: 
					System.out.println("--------- \nBuscar los tiempos de espera que tienen una desviación estándar en un rango dado y que son del primer trimestre del 2018:");
					System.out.println("Ingresar desviacion estandar inferior:");
					tiempoInferior =lector.nextDouble();
					System.out.println("Ingresar desviacion estandar superior:");
					tiempoSuperior =lector.nextDouble();
					System.out.println("Ingresar numero de viajes que se desea consultar:");
					numViajes = lector.nextInt();
					ArrayList<TravelTime> viajes=modelo.req3B(tiempoInferior, tiempoSuperior, numViajes);
					for(TravelTime t: viajes)
					{
						System.out.println("Zona de origen "+t.darSourceID());
						System.out.println("Zona de destino "+t.darDstID());
						System.out.println("Mes "+t.darTiempo());
						System.out.println("Desviacion estandar "+t.darDevTravelTime());
					}
					System.out.println("---------");
					break;
					
				case 8: 
					System.out.println("--------- \nRetornar todos los tiempos de viaje promedio que salen de una zona dada y a una hora dada:");
					System.out.println("Ingresar zona de salida:");
					int zona =lector.nextInt();
					System.out.println("Ingresar hora:");
					int ti =lector.nextInt();
					ArrayList<TravelTime> it=modelo.req1C(zona,ti);
					for(TravelTime t: it)
					{
						System.out.println("Zona de origen: "+ t.darSourceID());
						System.out.println("Zona de destino: "+t.darDstID());
						System.out.println("Hora: "+ t.darTiempo());
						System.out.println("Tiempo promedio de viaje: "+ t.darMeanTravelTime());
					}
					System.out.println("---------");
					break;
					
				case 9: 
					System.out.println("--------- \nRetornar todos los tiempos de viaje que llegan de una zona dada y en un rango de horas:");
					System.out.println("Ingresar zona de llegada:");
					int zonaS =lector.nextInt();
					System.out.println("Ingresar hora inferior:");
					int tinf =lector.nextInt();
					System.out.println("Ingresar hora superior:");
					int tsup =lector.nextInt();
					ArrayList<TravelTime> a=modelo.req2C(zonaS, tsup, tinf);
					for(TravelTime t: a)
					{
						System.out.println("Zona de origen: "+ t.darSourceID());
						System.out.println("Zona de destino: "+t.darDstID());
						System.out.println("Hora: "+ t.darTiempo());
						System.out.println("Tiempo promedio de viaje: "+ t.darMeanTravelTime());
					}
					System.out.println("---------");
					break;
					
				case 10: 
					System.out.println("--------- \nObtener las N zonas priorizadas por la mayor cantidad de nodos que definen su frontera:");
					System.out.println("Ingresar numero de zonas que se desea consultar:");
					numZonas = lector.nextInt();
					System.out.println("\nZonas con mayor cantidad de nodos:");
					if (numZonas > 20){numZonas = 20;}
					Iterator<String> req3C = modelo.req3C(numZonas).iterator();
					i = 1;
					while (req3C.hasNext())
					{
						System.out.println("\t" + i + ". " + req3C.next());
						i ++;
					}
					System.out.println("---------");
					break;
					
				case 11: 
					System.out.println("--------- \nGráfica ASCII - Porcentaje de datos faltantes para el primer semestre 2018:");
					System.out.println(modelo.req4C());
					System.out.println("---------");
					break;
					
				case 12: 
					System.out.println("--------- \n Hasta pronto !! \n---------"); 
					lector.close();
					fin = true;
					break;

				default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
