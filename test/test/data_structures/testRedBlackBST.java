package test.data_structures;

import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;
import model.data_structures.Iterator;
import model.data_structures.RedBlackBST;

public class testRedBlackBST extends TestCase
{

	private RedBlackBST<Integer, Integer> arbol;
	@Before
	//arbol vacio
	public void setUp1() 
	{
		arbol= new RedBlackBST<Integer, Integer>();
	}
	//arbol con elementos
	public void setUp2()
	{
		arbol.put(1, 3);
		arbol.put(2, 4);
		arbol.put(3, 6);
	}
	
	public void testPutAndSize()
	{
		setUp1();
		
		arbol.put(2, 2);
		assertEquals(1, arbol.size());
		
		arbol.put(1, 1);
		assertEquals(2, arbol.size());
		
		arbol.put(3, 3);
		assertEquals(3, arbol.size());
		
		arbol.put(4, 4);
		assertEquals(4, arbol.size());
	}
	
	public void testPut()
	{
		setUp1();
		
		arbol.put(2, 2);
		assertEquals(2, (int)arbol.darRaiz().darVal());
		
		arbol.put(1, 1);
		assertEquals(2, (int)arbol.darRaiz().darVal());
		assertEquals(1, (int)arbol.darRaiz().darIzq().darVal());
		assertEquals(false, arbol.darRaiz().esRojo());
		assertEquals(true, arbol.darRaiz().darIzq().esRojo());
		
		arbol = new RedBlackBST<Integer, Integer>();
		
		arbol.put(1, 1);
		assertEquals(1, (int)arbol.darRaiz().darVal());
		
		arbol.put(2, 2);
		assertEquals(2, (int)arbol.darRaiz().darVal());
		assertEquals(1, (int)arbol.darRaiz().darIzq().darVal());
		assertEquals(false, arbol.darRaiz().esRojo());
		assertEquals(true, arbol.darRaiz().darIzq().esRojo());
		
		arbol = new RedBlackBST<Integer, Integer>();
		
		arbol.put(2, 2);
		arbol.put(1, 1);
		
		arbol.put(3, 3);
		assertEquals(false, arbol.darRaiz().esRojo());
		assertEquals(false, arbol.darRaiz().darIzq().esRojo());
		assertEquals(1, (int)arbol.darRaiz().darIzq().darVal());
		assertEquals(false, arbol.darRaiz().darDer().esRojo());
		assertEquals(3, (int)arbol.darRaiz().darDer().darVal());
		
		arbol = new RedBlackBST<Integer, Integer>();
		
		arbol.put(3, 3);
		arbol.put(2, 2);
		
		arbol.put(1, 1);
		assertEquals(false, arbol.darRaiz().esRojo());
		assertEquals(false, arbol.darRaiz().darIzq().esRojo());
		assertEquals(1, (int)arbol.darRaiz().darIzq().darVal());
		assertEquals(false, arbol.darRaiz().darDer().esRojo());
		assertEquals(3, (int)arbol.darRaiz().darDer().darVal());
		
		arbol = new RedBlackBST<Integer, Integer>();
		
		arbol.put(3, 3);
		arbol.put(1, 1);
		
		arbol.put(2, 2);
		assertEquals(false, arbol.darRaiz().esRojo());
		assertEquals(false, arbol.darRaiz().darIzq().esRojo());
		assertEquals(1, (int)arbol.darRaiz().darIzq().darVal());
		assertEquals(false, arbol.darRaiz().darDer().esRojo());
		assertEquals(3, (int)arbol.darRaiz().darDer().darVal());
	}
	
	public void testIsEmpty()
	{
		setUp1();
		
		assertTrue(arbol.isEmpty());
		
		arbol.put(2, 2);
		
		assertFalse(arbol.isEmpty());
	}
	
	public void testHeight()
	{
		setUp1();
		
		assertEquals(0, arbol.height());
		
		arbol.put(62, 62);
		assertEquals(1, arbol.height());
		arbol.put(58, 58);
		assertEquals(2, arbol.height());
		arbol.put(33, 33);
		assertEquals(2, arbol.height());
		arbol.put(86, 86);
		assertEquals(3, arbol.height());
		arbol.put(56, 56);
		assertEquals(3, arbol.height());
		arbol.put(8, 8);
		assertEquals(3, arbol.height());
		arbol.put(63, 63);
		assertEquals(3, arbol.height());
		arbol.put(92, 92);
		assertEquals(4, arbol.height());
		arbol.put(73, 73);
		assertEquals(4, arbol.height());
		arbol.put(88, 88);
		assertEquals(4, arbol.height());
		arbol.put(64, 64);
		assertEquals(5, arbol.height());
	}
	
	public void testGetAndContains()
	{
		setUp1();
		
		arbol.put(62, 62);
		arbol.put(58, 58);
		arbol.put(33, 33);
		arbol.put(86, 86);
		arbol.put(56, 56);
		arbol.put(8, 8);
		arbol.put(63, 63);
		arbol.put(92, 92);
		arbol.put(73, 73);
		arbol.put(88, 88);
		arbol.put(64, 64);
		arbol.put(333, 444);
		
		assertTrue(arbol.contains(33));
		assertFalse(arbol.contains(34));
		assertTrue(arbol.contains(63));
		assertFalse(arbol.contains(65));
		assertTrue(arbol.contains(88));
		assertFalse(arbol.contains(89));
		
		assertEquals(56, (int)arbol.get(56));
		assertEquals(73, (int)arbol.get(73));
		assertEquals(444, (int)arbol.get(333));
	}
	
	public void testMinAndMax()
	{
		setUp1();
		
		arbol.put(62, 62);
		assertEquals(62, (int)arbol.min());
		assertEquals(62, (int)arbol.max());
		arbol.put(58, 58);
		arbol.put(33, 33);
		arbol.put(86, 86);
		arbol.put(56, 56);
		arbol.put(8, 8);
		arbol.put(63, 63);
		arbol.put(92, 92);
		arbol.put(73, 73);
		arbol.put(88, 88);
		arbol.put(64, 64);
		arbol.put(333, 444);
		
		assertEquals(8, (int)arbol.min());
		assertEquals(333, (int)arbol.max());
	}
	
	public void testKeys()
	{
		setUp1();
		
		arbol.put(62, 62);
		arbol.put(58, 58);
		arbol.put(33, 33);
		arbol.put(86, 86);
		arbol.put(56, 56);
		arbol.put(8, 8);
		arbol.put(63, 63);
		arbol.put(92, 92);
		arbol.put(73, 73);
		arbol.put(88, 88);
		arbol.put(64, 64);
		arbol.put(333, 444);
		
		Iterator<Integer> iter = arbol.keys();
		assertEquals(8, (int)iter.next());
		assertEquals(33, (int)iter.next());
		assertEquals(56, (int)iter.next());
		assertEquals(58, (int)iter.next());
		assertEquals(62, (int)iter.next());
		assertEquals(63, (int)iter.next());
		assertEquals(64, (int)iter.next());
		assertEquals(73, (int)iter.next());
		assertEquals(86, (int)iter.next());
		assertEquals(88, (int)iter.next());
		assertEquals(92, (int)iter.next());
		assertEquals(333, (int)iter.next());
	}
	
	public void testValuesInRangeAndKeysInRange()
	{
		setUp1();
		
		arbol.put(62, 62);
		arbol.put(58, 58);
		arbol.put(33, 33);
		arbol.put(86, 86);
		arbol.put(56, 56);
		arbol.put(8, 8);
		arbol.put(63, 63);
		arbol.put(92, 92);
		arbol.put(73, 73);
		arbol.put(88, 88);
		arbol.put(64, 64);
		arbol.put(333, 444);
		
		Iterator<Integer> iter = arbol.valuesInRange(1, 60);
		assertEquals(8, (int)iter.next());
		assertEquals(33, (int)iter.next());
		assertEquals(56, (int)iter.next());
		assertEquals(58, (int)iter.next());
		assertFalse(iter.hasNext());
		
		iter = arbol.keysInRange(1, 60);
		assertEquals(8, (int)iter.next());
		assertEquals(33, (int)iter.next());
		assertEquals(56, (int)iter.next());
		assertEquals(58, (int)iter.next());
		assertFalse(iter.hasNext());
	}
	
	public void testValuesOfKeysInRange()
	{
		RedBlackBST<Integer, Character> arbol1 = new RedBlackBST<>();
		arbol1.put(62, 'Z');
		arbol1.put(58, 'O');
		arbol1.put(33, 'E');
		arbol1.put(86, 'Q');
		arbol1.put(56, 'R');
		arbol1.put(8, 'P');
		arbol1.put(63, 'W');
		arbol1.put(92, 'V');
		arbol1.put(73, 'C');
		arbol1.put(88, 'I');
		arbol1.put(64, 'G');
		arbol1.put(333, 'T');
		
		Iterator<Character> iter = arbol1.valuesOfKeysInRange(1, 58);
		assertEquals('P', (int)iter.next());
		assertEquals('E', (int)iter.next());
		assertEquals('R', (int)iter.next());
		assertEquals('O', (int)iter.next());
		assertFalse(iter.hasNext());
	}
}
