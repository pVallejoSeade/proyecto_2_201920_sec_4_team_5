package test.data_structures;


import junit.framework.TestCase;

import model.data_structures.MaxHeapCP;
import model.logic.MVCModelo;
import model.logic.TravelTime;

public class PruebaColaPrioridad extends TestCase
{	
	private MaxHeapCP<Integer> heapCP;
	
	public void testDarNumElementosYAgregar()
	{
		MaxHeapCP<Integer> heap = new MaxHeapCP<>();
		
		assertEquals(0, heap.darNumElementos());
		
		heap.agregar(1);
		assertEquals(1, heap.darNumElementos());
		heap.agregar(2);
		assertEquals(2, heap.darNumElementos());
		heap.agregar(3);
		assertEquals(3, heap.darNumElementos());
		heap.agregar(4);
		assertEquals(4, heap.darNumElementos());
	}
	
	public void testEsVacia()
	{
		MaxHeapCP<Integer> heap = new MaxHeapCP<>();
		
		assertTrue(heap.esVacia());
		
		heap.agregar(1);
		assertFalse(heap.esVacia());
	}
	
	public void testSacarMax()
	{
		MaxHeapCP<Integer> heap = new MaxHeapCP<>();
		
		heap.agregar(824);
		heap.agregar(83);
		heap.agregar(760);
		heap.agregar(283);
		heap.agregar(419);
		
		assertEquals(824, (int)heap.sacarMax());
		assertEquals(760, (int)heap.sacarMax());
		assertEquals(419, (int)heap.sacarMax());
		assertEquals(283, (int)heap.sacarMax());
		assertEquals(83, (int)heap.sacarMax());
		assertTrue(heap.esVacia());
		
		MaxHeapCP<String> heap1 = new MaxHeapCP<>();
		
		heap1.agregar("Pedro");
		heap1.agregar("Pablo");
		heap1.agregar("Emma");
		heap1.agregar("Sofia");
		heap1.agregar("Martin");
		heap1.agregar("Andres");
		heap1.agregar("Mateo");
		heap1.agregar("Alejandro");
		
		assertEquals("Sofia", heap1.sacarMax());
		assertEquals("Pedro", heap1.sacarMax());
		assertEquals("Pablo", heap1.sacarMax());
		assertEquals("Mateo", heap1.sacarMax());
		assertEquals("Martin", heap1.sacarMax());
		assertEquals("Emma", heap1.sacarMax());
		assertEquals("Andres", heap1.sacarMax());
		assertEquals("Alejandro", heap1.sacarMax());
		assertTrue(heap1.esVacia());
		
	}
	
}

