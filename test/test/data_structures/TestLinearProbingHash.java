package test.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Iterator;
import model.data_structures.LinearProbingHashT;


public class TestLinearProbingHash 
{

	private LinearProbingHashT<Integer, Integer> tabla;
	
	@Before
	public void setUp1() {
		tabla= new LinearProbingHashT<Integer, Integer>(1);
	}
	
	
	@Test
	public void testLinearProbing() {
		
		assertTrue(tabla!=null);
		assertEquals(0,tabla.darN());
		assertEquals(1, tabla.darM());
	}
	
	
	@Test
	public void testPut()
	{
		tabla.put(10, 1);
		assertTrue(tabla.get(10)==1);
		assertTrue(tabla.darN()==1);
		tabla.put(2, 9);
		assertTrue(tabla.get(2)==9);
		assertTrue(tabla.darN()==2);
	}

	@Test
	public void testRehash() //Resize
	{
		tabla.resize(3);
		assertEquals(3, tabla.darM());
		assertEquals(0,tabla.darN());
		tabla.put(1, 1);
		assertEquals(3, tabla.darM());
		assertEquals(1,tabla.darN());
		tabla.put(4, 3);
		tabla.put(5, 1);
		assertEquals(6, tabla.darM());
	}
	
	@Test
	public void testPutInSet()
	{
		tabla.resize(4);
		tabla.putInSet(2, 1);
		assertEquals(4, tabla.darM());
		assertEquals(1,tabla.darN());
		
		tabla.putInSet(2, 3);
		assertEquals(4, tabla.darM());
		assertEquals(2,tabla.darN());
		assertTrue(tabla.getSet(2)!=null);
		
		tabla.putInSet(2, 7);
		Iterator it=tabla.getSet(2);
		assertEquals(1,it.next());
		assertEquals(3,it.next());
		assertEquals(7,it.next());
	}
	
	@Test
	public void testGet()
	{
		assertTrue(tabla.get(10)==null);
		tabla.putInSet(2, 3);
		tabla.putInSet(4, 3);
		tabla.putInSet(6, 3);
		assertTrue(tabla.get(2)==3);
		assertTrue(tabla.get(4)==3);
		assertTrue(tabla.get(6)==3);		
	}
	
	@Test
	public void testGetSet()
	{
		assertTrue(tabla.getSet(10).hasNext()==false);
		
		tabla.putInSet(1, 1);
		tabla.putInSet(1, 2);
		tabla.putInSet(1, 3);
		tabla.putInSet(1, 5);
		
		Iterator it=tabla.getSet(1);
		assertEquals(1,it.next());
		assertEquals(2,it.next());
		assertEquals(3,it.next());
		assertEquals(5,it.next());
	}
	
	@Test
	public void testDelete()
	{
		assertTrue(tabla.delete(4)==null);
		
		tabla.put(3, 4);
		tabla.put(4, 4);
		tabla.put(5, 4);
		tabla.put(6, 4);
		
		assertTrue(tabla.darN()==4);
		assertTrue(tabla.delete(5)==4);
		assertTrue(tabla.darN()==3);
		assertTrue(tabla.delete(6)==4);
		assertTrue(tabla.darN()==2);
		
	}
	
	@Test
	public void testDeleteSet()
	{
		assertTrue(tabla.deleteSet(10).hasNext()==false);
		
		tabla.putInSet(3, 4);
		tabla.putInSet(3, 3);
		tabla.putInSet(3, 6);
		tabla.putInSet(3, 8);
		
		Iterator  it=tabla.deleteSet(3);
		assertEquals(4,it.next());
		assertEquals(3,it.next());
		assertEquals(6,it.next());
		assertEquals(8,it.next());
	}
	
	public void testKeys()
	{		
		tabla.put(1,1);
		tabla.put(8,8);
		tabla.put(5,5);

		Iterator<Integer> iter = tabla.keys();
		boolean esta1 = false;
		boolean esta8 = false;
		boolean esta5 = false;
		
		while (iter.hasNext())
		{
			int llave = iter.next();
			if (llave == 1) esta1 = true;
			else if (llave == 8) esta8 = true;
			else if (llave == 5) esta5 = true;
		}
		
		assertTrue(esta1);
		assertTrue(esta8);
		assertTrue(esta5);
	}
	
}
